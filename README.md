<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [前端复杂组件库](#%E5%89%8D%E7%AB%AF%E5%A4%8D%E6%9D%82%E7%BB%84%E4%BB%B6%E5%BA%93)
    - [开发背景](#%E5%BC%80%E5%8F%91%E8%83%8C%E6%99%AF)
  - [目前包括](#%E7%9B%AE%E5%89%8D%E5%8C%85%E6%8B%AC)
    - [仿钉钉自定义流程图（完成）；dingding分支](#%E4%BB%BF%E9%92%89%E9%92%89%E8%87%AA%E5%AE%9A%E4%B9%89%E6%B5%81%E7%A8%8B%E5%9B%BE%E5%AE%8C%E6%88%90dingding%E5%88%86%E6%94%AF)
    - [echarts 地图添加点击事件显示状图（完成）； echarts分支](#echarts-%E5%9C%B0%E5%9B%BE%E6%B7%BB%E5%8A%A0%E7%82%B9%E5%87%BB%E4%BA%8B%E4%BB%B6%E6%98%BE%E7%A4%BA%E7%8A%B6%E5%9B%BE%E5%AE%8C%E6%88%90-echarts%E5%88%86%E6%94%AF)
    - [SelectBox穿梭框选人组件（完成）selectBox分支](#selectbox%E7%A9%BF%E6%A2%AD%E6%A1%86%E9%80%89%E4%BA%BA%E7%BB%84%E4%BB%B6%E5%AE%8C%E6%88%90selectbox%E5%88%86%E6%94%AF)
- [仿钉钉自定义流程文档](#%E4%BB%BF%E9%92%89%E9%92%89%E8%87%AA%E5%AE%9A%E4%B9%89%E6%B5%81%E7%A8%8B%E6%96%87%E6%A1%A3)
  - [效果图](#%E6%95%88%E6%9E%9C%E5%9B%BE)
  - [使用方法](#%E4%BD%BF%E7%94%A8%E6%96%B9%E6%B3%95)
    - [Attributes](#attributes)
    - [Events](#events)
    - [方法](#%E6%96%B9%E6%B3%95)
    - [TreeNode](#treenode)
- [SelectBox  树状选择组件](#selectbox--%E6%A0%91%E7%8A%B6%E9%80%89%E6%8B%A9%E7%BB%84%E4%BB%B6)
  - [废话不多说 先贴图](#%E5%BA%9F%E8%AF%9D%E4%B8%8D%E5%A4%9A%E8%AF%B4-%E5%85%88%E8%B4%B4%E5%9B%BE)
    - [树状选择效果图](#%E6%A0%91%E7%8A%B6%E9%80%89%E6%8B%A9%E6%95%88%E6%9E%9C%E5%9B%BE)
    - [列表多选效果图](#%E5%88%97%E8%A1%A8%E5%A4%9A%E9%80%89%E6%95%88%E6%9E%9C%E5%9B%BE)
  - [开始废话](#%E5%BC%80%E5%A7%8B%E5%BA%9F%E8%AF%9D)
    - [开发背景](#%E5%BC%80%E5%8F%91%E8%83%8C%E6%99%AF-1)
  - [接下来是组件的使用文档](#%E6%8E%A5%E4%B8%8B%E6%9D%A5%E6%98%AF%E7%BB%84%E4%BB%B6%E7%9A%84%E4%BD%BF%E7%94%A8%E6%96%87%E6%A1%A3)
- [selectBox](#selectbox)
    - [使用方法](#%E4%BD%BF%E7%94%A8%E6%96%B9%E6%B3%95-1)
    - [Attributes](#attributes-1)
    - [Events](#events-1)
    - [props](#props)
    - [TreeNode](#treenode-1)
    - [iconConfig](#iconconfig)
    - [searchConfig](#searchconfig)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->




# 前端复杂组件库
### 开发背景
 
  随着前端技术的不断发展，不仅技术栈越来越稳定，各大知名UI库也被广泛应用，其中Element-ui Iview Ant-design 被各大公司在后台管理项目开发中广泛应用。
  为了保持组件的原子性，基本所有大型UI库都把各个组件分离开来。 然而 在后台管理系统的开发过程中，有很多功能的完成，需要基于多种组件。 故而，在这里把自己遇到的，开发的，通用的复杂的功能能，总结起来并与业务解耦，开源出一份开箱即用的 前端复杂功能组件库
##  目前包括 

### 仿钉钉自定义流程图（完成）；dingding分支
### echarts 地图添加点击事件显示状图（完成）； echarts分支

###  SelectBox穿梭框选人组件（完成）selectBox分支

需要的同学可以按需拉取


# 仿钉钉自定义流程文档

## 效果图
<img src='flow.png'/>

## 使用方法
在需要的地方引入，例如全局引入 (main.js)
```javaScript
import DrawFlow from "./components/DrawFlow";
Vue.use(DrawFlow);
```
然后就使用它
```html
 <FactoryDrawFlow
      @clickNode="clickNode"
      ref="flow"
      :FlowConfig="FlowConfig"
    ></FactoryDrawFlow>
```
### Attributes 
| 参数       | 说明     | 类型       | 可选值 | 默认值 |
|------------|--------|------------|--------|--------|
| FlowConfig | 展示数据 | FlowConfig | --     | --     |
### Events

| 事件名称   | 说明   | 回调参数  
|------|------|------|
| clickNode | 点击流程节点时触发 | 返回当前节点数据（TreeNode）

### 方法

| 方法名称   | 说明   | 回调参数  
|------|------|------|
| getResData | 获取流程图数据 | 返回两个参数，依次为：所有流程节点的一维数组集合、用于渲染流程图而生成的树状结构
| nodeChange | 在自己业务页面操作完，当前节点发生变化时，可手动去触发 | 支持传入当前改变的节点，数据结构参考下文TreeNode

### TreeNode

| 参数     | 说明                                          | 类型            | 可选值                  | 默认值 |
|----------|---------------------------------------------|-----------------|-------------------------|--------|
| id       | 定位节点的唯一标识                            | string          | --                      | --     |
| groupId  | 流程图生成过程中生成的组id                    | string          | --                      | --     |
| type     | 节点类型                                      | string          | '1','2','3','4','5','6' | --     |
| title    | 标题                                          | string          | --                      | --     |
| groupPid | 流程图渲染过程中所应用数据                    | string          | --                      | --     |
| content  | 节点显示内容                                  | string          | --                      | --     |
| isRow    | 是否为行元素                                  | boolean         | --                      | --     |
| isRoot   | 是否为根节点                                  | boolean         | --                      | --     |
| data     | 拓展属性，用以存储业务相关内容                 | object          | --                      | {}     |
| pids     | 流程图生成过程中生成属性，指明此节点父节点数组 | Array<'string'> | --                      | --     |

# SelectBox  树状选择组件
## 废话不多说 先贴图
[这里是git地址 ](https://gitee.com/xiaoyaoluntian/imitating-dingding-flow-chart/tree/selectBoxCom/)

 ### 树状选择效果图
<img src="./selectBox.jpg"/>

 ### 列表多选效果图
<img src="./selectList.jpg" />

## 开始废话
### 开发背景 
  众所周知，大部分企业的管理后台的开发离不开权限管理，一般来说，我们会分析各种权限把他们量化为资源，然后分配到各个角色/部门中，然后再给用户赋予各种角色或者挂载到各个部门。 如果权限设计的足够复杂，第三方UI常用的穿梭框等组件就不能完美的满足我们的需求，进而需要开发人员去根据需求，寻找多个组件组合使用。 但是，里面的业务逻辑万变不离其宗。 接下来 介绍一下笔者写的这款树状选择组件。 
  简单来说组件分为左右两个模块 左侧为选择树（例如 部门-角色树  用户-权限树）或者多选列表。 右侧为已选数据。 点击保存 会返回三个数据，依次为 本次修改新增的数组ID 本次修改删除的数组ID 以及 所选择的所有数据ID集合

最后给我的老东家打个广告，金现代企业股份有限公司做的项目真的不错，在里面带的一年里，成长很多。在此感恩
## 接下来是组件的使用文档

# selectBox

### 使用方法 

```javascript
    // 首先把组件从git上down下来 然后引用到项目里，在项目里use一下
    import selectBox from "@/components/selectBox";
    Vue.use(selectBox);
```
```html
    <select-box
      ref="selectBox"
      :allNode="relatePermConfig.allNode"
      :boxDialogVisible="relatePermConfig.editVisible"
      :checkedNode="relatePermConfig.checked"
      :iconConfig="relatePermConfig.iconConfig"
      :rightKeyQ="relatePermConfig.rightKeyQ"
      :defaultProps="relatePermConfig.defaultProps"
      :searchConfig="relatePermConfig.search"
      :title="relatePermConfig.title"
      @close="relatePermConfig.editVisible = false"
      @save="addAndRemovePermPtag"
    />
```
### Attributes 
| 参数             | 说明                                                                                    | 类型            | 是否必填 | 可选值        | 默认值       |
|------------------|---------------------------------------------------------------------------------------|-----------------|----------|---------------|--------------|
| allNode          | 所有的节点数据                                                                          | Array[TreeNode] | true     | --            | []           |
| checkedNode      | 已选择的节点Id集合                                                                      | Array[String]   | true     | --            | []           |
| defaultProps     | 配置选项，具体看下表                                                                     | Object          | false    | --            | --           |
| listStyle        | 右侧展示方式类型                                                                        | String          | false    | 'tree'/'list' | 'tree'       |
| boxDialogVisible | 是否展示弹出框                                                                          | Boolean         | true     | true/false    | false        |
| iconConfig       | 图标配置（里面有必填属性，起名不准确，之后会改，是一个依赖项）                               | iconConfig      | true     | --            | iconConfig   |
| title            | 对话框标题                                                                              | String          | true     | --            | "权限控制"   |
| rightKey         | 右侧列表显示所对应数据里面key的属性（本来默与左侧显示一样，但确实有不一样的场景，就很蛋疼） | String          | false    | --            | "name"       |
| searchConfig     | 搜索框配 见下表置                                                                       | Object          | false    | --            | searchConfig |
| indep            | 树状图时叶子节点改变时，是否不与父节点产生关联                                           | Boolean         | false    | true/false    | false        |
### Events

| 事件名称   | 说明   | 回调参数  
|------|------|------|
| close | 当组件关闭时触发 | null
| save | 点击保存时触发 | 返回三个参数(addList,deleteList,checkedNode)，依次为:本次修改后应该新增的Id数组，本次修改后应该删除的Id数组，本次修改后所有选中的数据集合

### props 
| 参数     | 说明                                                    | 类型                          | 是否必填 | 可选值 | 默认值     |
|----------|-------------------------------------------------------|-------------------------------|----------|--------|------------|
| label    | 指定节点标签为节点对象的某个属性值                      | string, function(data, node)  | true     | --     | 'id'       |
| children | 指定子树为节点对象的某个属性值                          | string                        | true     | --     | 'children' |
| disabled | 指定节点选择框是否禁用为节点对象的某个属性值            | boolean, function(data, node) | true     | --     | --         |
| isLeaf   | 指定节点是否为叶子节点，仅在指定了 lazy 属性的情况下生效 | boolean, function(data, node) | true     | --     | --         |
### TreeNode 
| 参数 | 说明                                                                             | 类型   | 是否必填 | 可选值 | 默认值 |
|------|--------------------------------------------------------------------------------|--------|----------|--------|--------|
| id   | 默认的节点唯一标识，如果需要更改 请参照element-ui的tree组件的props属性，或者看上表 | String | true     | --     | --     |
| name | 左侧展示值，如果需要更改 请参照element-ui的tree组件的props属性，或者看上表         | String | true     | --     | --     |
| type | 节点类型，用来过滤父级节点                                                        | String | true     | --     | --     |
| pid  | 父节点ID，如果是根节点请设置为null                                                | String | true     | --     | --     |

### iconConfig
| 参数         | 说明                         | 类型   | 是否必填 | 可选值 | 默认值                                      |
|--------------|----------------------------|--------|----------|--------|---------------------------------------------|
| parentType   | 左侧为树状图时父节点图标配置 | Object | true     | --     | {key:'pre',icon:'el-icon-tree-rGroup'}      |
| childrenType | 左侧为树状图时子节点图标配置 | Object | true     | --     | {key:'permission',icon:'el-icon-tree-role'} |

 在这里解释一下，组件里会根据父节点配置的key的值，与节点数据里面的type做对比，从而过滤掉选中的父节点。
因为很多业务场景下，右侧的列表只希望展示选中的子节点，
 如果需要展示选中的父级节点，可以把parentType里面的key设置为空

### searchConfig
| 参数  | 说明                                                  | 类型    | 是否必填 | 可选值     | 默认值   |
|-------|-----------------------------------------------------|---------|----------|------------|----------|
| title | 其实就是placeholder啦~                                | String  | false    | --         | '请输入' |
| http  | 这是你项目中搜索所调用的接口，接受一个默认参数（params） | Promise | true     | --         | --       |
| show  | 是否显示搜索框,如果是false，其他的都可以不传           | Boolean | false    | true/false | true     |
| key   | 搜索时用到的关键词                                    | String  | true     | --         | 'name'   |
```javaScript
// 可能比较难理解，在这里演示一下 searchConfig，在项目里有demo 可以拉下来看一下
/** 
 *  例如你的调用查询接口为 this.$axios  ，且this.$axios接受一个参数 
    参数形式为 
    @params : data:{params:{key:value}}
    @key ： searchConfig所配置的key
*/
const getData = params => {
    this.$axios(params).then(res=>{

    })
};
let searchConfig = {
    http:this.getData,
    key:'name'
}

```

"# full_work_flow"  
"# full_work_flow" 
